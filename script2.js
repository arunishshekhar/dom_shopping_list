class User {
    constructor(username) {
        this.username = username;
        this.data = [];
    }

    add_product(item) {
        this.data.push(new Product(item))
    }
}

class Product {
    constructor(product_name) {
        this.product_name = product_name;
        this.is_bought = false;
        this.is_deleted = false;
    }
}

let users_list = [];

let user;

function first_action() {
    let username_data = document.getElementById('username').value;
    if (username_data) {
        let data;
        if (users_list.find((data) => {
            if (data.username === username_data) {
                user = data;
                return true;
            }
        })) {
            console.log("Welcome Back");
            map_data();
        }
        else {
            user = new User(username_data);
            users_list.push(user);
            map_data();
        }
    }
}

function add_item() {
    const item_data = document.getElementById('itemname').value;
    if (item_data) {
        first_action();
        user.add_product(item_data);
        document.getElementById('itemname').value = "";
        map_data();
    }
}

function map_data() {
    const list_div = document.getElementById('all-content');
    let item_to_put = ""
    user.data.map((element) => {
        let temp_child = "";
        let class_name = "";
        if (element.is_deleted) {
            class_name = "deleted-indivisual-content";
        }
        else if (element.is_bought) {
            class_name = "purchased-indivisual-content";
        }
        else {
            class_name = "indivisual-content";
        }
        temp_child = `<section class= ${class_name}>
        <h3>${element.product_name}</h3>
        <p>${user.username}</p>
        <button class="bought-button" onclick="product_bought(this)">Bought</button>
        <button class="delete-button" onclick="delete_item(this)">Delete</button>
    </section>`
        item_to_put += temp_child;
    })
    list_div.innerHTML = item_to_put;
}

function delete_item (data) {
    let p_bought = data.parentElement;
    let temp_data = p_bought.innerText.split("\n")[0];
    user.data.find((indi) => {
        if (indi.product_name === temp_data) {
            indi.is_deleted = true;
        }
    })
    map_data();
}

function product_bought (data) {
    let p_bought = data.parentElement;
    let temp_data = p_bought.innerText.split("\n")[0];
    user.data.find((indi) => {
        if (indi.product_name === temp_data) {
            indi.is_bought = true;
        }
    })
    map_data();
}
