let users = [
    {
        username: 'Arun',
        data: []
    },
    {
        username: 'Roy',
        data: []
    }
]

let sample_user = {
    username: null,
    data: []
}

let sample_data = {
    title: null,
    bought: false,
    deleted:false
}

function first_action() {
    const username_data = document.getElementById('username').value;
    let user_data = gatherdata(username_data);
    show_list(user_data);
}

function gatherdata(username_data = document.getElementById('username').value) {
    let user_data = users.find((user) => {
        if (user.username === username_data) {
            return user;
        }
    })
    if (user_data) {
        return user_data;
    }
    else {
        let temp_user = Object.create(sample_user);
        temp_user.username = username_data;
        temp_user.data = [];
        users.push(temp_user);
        return temp_user;
    }
}

function update_list() {
    const username_data = document.getElementById('username').value;
    let user_data = gatherdata(username_data);
    const item_data = document.getElementById('itemname').value;
    let temp_data = Object.create(sample_data);
    temp_data.title = item_data;
    temp_data.bought = false;
    user_data.data.push(temp_data);
    console.log(users);
    show_list(user_data);
}

function show_list(user_data = gatherdata(username_data)) {
    const list_div = document.getElementById('all-content');
    let item_to_put = ""
    user_data.data.forEach(element => {
        let temp_child = "";
        if (user_data.bought) {
            temp_child = `<section class="purchased.indivisual-content">
                                    <h3>${element.title}</h3>
                                    <p>${user_data.username}</p>
                                    <button class="bought-button" onclick="product_bought(this)">Bought</button>
                                    <button class="delete-button" onclick="delete_item(this)">Delete</button>
                                </section>`

        }
        else {
            temp_child = `<section class="indivisual-content">
                                    <h3>${element.title}</h3>
                                    <p>${user_data.username}</p>
                                    <button class="bought-button" onclick="product_bought(this)">Bought</button>
                                    <button class="delete-button" onclick="delete_item(this)">Delete</button>
                                </section>`
        }
        item_to_put+=temp_child;
    });
    list_div.innerHTML = item_to_put;
}

function delete_item (section_to_delete) {
    section_to_delete.parentElement.remove();
}

function product_bought (section_bought) {
    let p_bought = section_bought.parentElement;
    let temp_data = gatherdata(p_bought.innerText.split("\n")[2]);
    console.log(temp_data)
    temp_data.find((indi_cont) => {
        if (indi_cont.title === p_bought.innerText.split("\n")[0])
        {
            indi_cont.bought = true;
        }
    })
    show_list()
}

function filter_object () {
    
    let filtered_data = users.filter((user) => {
        user.data
        console.log(user);
    })
}

filter_object()